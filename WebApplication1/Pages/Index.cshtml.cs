﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using JWT.Algorithms;
using JWT.Builder;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace WebApplication1.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly string _token = GenerateAccessToken();
        public IList<Label> labels = new List<Label>();
        public IList<Package> packages = new List<Package>();
        private readonly string webserver = "https://warm-castle-36416.herokuapp.com";
        private readonly IHttpClientFactory _clientFactory;
        public IndexModel(ILogger<IndexModel> logger, IHttpClientFactory clientFactory)
        {
            _logger = logger;
            _logger.LogInformation(_token);
            _clientFactory = clientFactory;
            labels = GetLabels();
            packages = GetPackages();
        }

        public IActionResult OnPostCreate(string label, string user)
        {
            var request = new HttpRequestMessage(HttpMethod.Post,
                $"{webserver}/courier/packages/{label}?user={user}");

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            var response = client.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("cannot connect to server");
            }

            return RedirectToPage("Index");
        }

        public IActionResult OnPostChangeStatus(string label, string user)
        {
            var newStatus = Request.Form["newstaus"].ToString();
            var url = $"{webserver}/courier/packages/{label}?state={newStatus}&user={user}";
            var request = new HttpRequestMessage(HttpMethod.Put, url);

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            var response = client.SendAsync(request).Result;
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("cannot connect to server");
            }

            return RedirectToPage("Index");
        }

        public IList<Label> GetLabels()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{webserver}/courier/labels");

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            var response = client.SendAsync(request).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                JObject Jcontent = JObject.Parse(content);
                IList<JToken> Jlabs = Jcontent["_embedded"]["items"].ToList();

                IList<Label> list = new List<Label>();
                foreach (JToken result in Jlabs)
                {
                    Label lab = result.ToObject<Label>();
                    list.Add(lab);
                }

                return list;
            }
            throw new HttpRequestException("cannot connect to server");
        }
        public IList<Package> GetPackages()
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
                $"{webserver}/courier/packages");

            var client = _clientFactory.CreateClient();
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", _token);

            var response = client.SendAsync(request).Result;
            if (response.IsSuccessStatusCode)
            {
                var content = response.Content.ReadAsStringAsync().Result;
                JObject Jcontent = JObject.Parse(content);
                IList<JToken> Jlabs = Jcontent["_embedded"]["items"].ToList();

                IList<Package> list = new List<Package>();
                foreach (JToken result in Jlabs)
                {
                    Package lab = result.ToObject<Package>();
                    list.Add(lab);
                }

                return list;
            }
            throw new HttpRequestException("cannot connect to server");
        }

        private static string GenerateAccessToken()
        {
            return new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(Encoding.ASCII.GetBytes("DF8WT34^$SF1Gh%^T@$1"))
                .AddClaim("exp", DateTimeOffset.UtcNow.AddHours(2).ToUnixTimeSeconds())
                .AddClaim("usr", "user")
                .AddClaim("user_role", "courier")
                .AddClaim("aud", "aud")
                .Encode();
        }
    }
}
